# ground-truth-jobs

This repository contains resources to create a Ground Truth job in AWS.

## Create necessary resources

- Create IAM role `d-ew1-mturk-test` with
    - Trust relationship with Sagemaker service
    - AmazonSageMakerFullAccess policy linked
    - Following custom policy linked:
    ```json
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:*"
                ],
                "Resource": [
                    "arn:aws:s3:::d-ew1-ted-ai-experiments-data",
                    "arn:aws:s3:::d-ew1-ted-ai-experiments-data/*"
                ]
            }
        ]
    }
    ```
- Create necessary AWS resources: `bash job.sh create-deps`

(`bash job.sh delete-deps` can be run to delete resources)

## Register user in Ground Truth workforce

- Go to Amazon SageMaker service > Ground Truth > Labeling workforces > Private
- Click on the private team, then "Workers", then "Add workers to team", then "Add workers to team"
- Enter email address(es) of the user and click on "Invite new workers"
- An email should have been sent to users with their username and a temporary password
- Click on the private team, then "Workers", then "Add workers to team"
- Select users to add and click on "Add workers to team"
- The user should now be able to connect to labeling interface

## Create Ground Truth job

- Put CSV gold standard dataset in `tmp/gold_standard_dataset.csv`
- Generates the corresponding manifest `python3 generate-manifest`
- Upload files inside `data` folder on S3 at `s3://d-ew1-ted-ai-experiments-data/CPVs/202306/ground-truth-labeling`.
- Create the job resource on AWS:
  `bash job.sh create-job 1 d-ew1-ted-ai-experiments-data CPVs/202306/ground-truth-labeling d-ew1-mturk-test`
- Note that if a user is logged in to the labeling portal, they must log back to view the new labeling job
