from pathlib import Path

import pandas as pd

if __name__ == '__main__':
    df = pd.read_csv("tmp/gold_standard_dataset.csv", index_col=0)
    df["title_texte"] = df["title_texte"].str.replace('"', '\\"')
    df["manifest"] = df.apply(lambda r: f'{{"id": "{r.name}", "source": "{r.name}: {r["title_texte"]}"}}', axis=1)
    (Path("data") / "dataset.manifest").write_text("\n".join(list(df["manifest"])))
