#!/usr/bin/env bash
#set -euo pipefail

####################################
### RESOURCE RETRIEVAL
####################################

function print_user_pool_id() {
  aws cognito-idp list-user-pools \
    --max-results 10 \
    --output text \
    --query 'UserPools[?Name==`'$RESOURCE_NAME'`].Id'
}

function print_user_pool_client_id() {
  aws cognito-idp list-user-pool-clients \
    --user-pool-id "$user_pool_id" \
    --output text \
    --query 'UserPoolClients[].ClientId'
}

function print_workteam_arn() {
  aws sagemaker describe-workteam \
    --workteam-name $RESOURCE_NAME \
    --output text \
    --query 'Workteam.WorkteamArn'
}

function print_workforce_domain() {
  aws sagemaker describe-workforce \
    --workforce-name $RESOURCE_NAME \
    --output text \
    --query 'Workforce.SubDomain'
}

####################################
### RESOURCE CREATION
####################################

function create_user_pool() {
  user_pool_id=$(
    aws cognito-idp create-user-pool \
      --pool-name "$RESOURCE_NAME" \
      --admin-create-user-config "AllowAdminCreateUserOnly=true,
        InviteMessageTemplate={
          SMSMessage='Your username for TED AI labeling is {username} and temporary password is {####}',
          EmailSubject='TED AI labeling credentials',
          EmailMessage='Username: {username}<br>Temporary password: {####}'
        }" \
      --output text \
      --query 'UserPool.Id'
  )
  aws cognito-idp create-user-pool-client \
    --user-pool-id "$user_pool_id" \
    --client-name "$RESOURCE_NAME" \
    --generate-secret \
    --output text
  aws cognito-idp create-user-pool-domain \
    --domain "$RESOURCE_NAME" \
    --user-pool-id "$user_pool_id"
  aws cognito-idp create-group \
    --group-name "$RESOURCE_NAME" \
    --user-pool-id "$user_pool_id"
}

function configure_user_pool_client() {
  user_pool_id=$1
  user_pool_client_id=$2
  workforce_domain=$(print_workforce_domain)
  aws cognito-idp update-user-pool-client \
    --user-pool-id "$user_pool_id" \
    --client-id "$user_pool_client_id" \
    --access-token-validity 1 \
    --id-token-validity 1 \
    --refresh-token-validity 30 \
    --token-validity-units "AccessToken=hours,IdToken=hours,RefreshToken=days" \
    --prevent-user-existence-errors LEGACY \
    --enable-token-revocation \
    --supported-identity-providers COGNITO \
    --allowed-o-auth-flows-user-pool-client \
    --allowed-o-auth-flows code implicit \
    --allowed-o-auth-scopes email openid \
    --explicit-auth-flows ALLOW_CUSTOM_AUTH ALLOW_USER_PASSWORD_AUTH ALLOW_USER_SRP_AUTH ALLOW_REFRESH_TOKEN_AUTH \
    --callback-urls "https://$workforce_domain/oauth2/idpresponse" \
    --logout-urls "https://$workforce_domain/logout" \
    --output text
}

function create_workforce() {
  user_pool_id=$1
  user_pool_client_id=$2
  aws sagemaker create-workforce \
    --workforce-name "$RESOURCE_NAME" \
    --cognito-config "UserPool=$user_pool_id,ClientId=$user_pool_client_id" \
    --output text
  while ! (aws sagemaker create-workteam \
    --workteam-name "$RESOURCE_NAME" \
    --workforce-name "$RESOURCE_NAME" \
    --description "Default work team" \
    --member-definitions "CognitoMemberDefinition={UserPool=$user_pool_id, UserGroup=$RESOURCE_NAME, ClientId=$user_pool_client_id}"); do
    echo "workforce not yet created"
    sleep 10
  done
}

function create_labeling_job() {
  workteam_arn=$1
  aws sagemaker create-labeling-job \
    --labeling-job-name "$RESOURCE_NAME-$JOB_SUFFIX" \
    --label-attribute-name label \
    --role-arn "$ROLE_ARN" \
    --input-config "DataSource={
        S3DataSource={
          ManifestS3Uri='s3://$S3_WORKING_BUCKET/$S3_WORKING_PATH/dataset.manifest'
        }
      },
      DataAttributes={
        ContentClassifiers=[FreeOfPersonallyIdentifiableInformation, FreeOfAdultContent]
      }" \
    --output-config "S3OutputPath=s3://$S3_WORKING_BUCKET/$S3_WORKING_PATH/output/" \
    --label-category-config-s3-uri "s3://$S3_WORKING_BUCKET/$S3_WORKING_PATH/labels.json" \
    --human-task-config "WorkteamArn=$workteam_arn,
      UiConfig={
        UiTemplateS3Uri='s3://$S3_WORKING_BUCKET/$S3_WORKING_PATH/template.html'
      },
      PreHumanTaskLambdaArn='arn:aws:lambda:eu-west-1:568282634449:function:PRE-TextMultiClassMultiLabel',
      TaskKeywords='Text Classification',
      TaskTitle='Multi-label text classification',
      TaskDescription='CPV classification of text',
      NumberOfHumanWorkersPerDataObject=$WORKER_COUNT,
      TaskTimeLimitInSeconds=3600,
      TaskAvailabilityLifetimeInSeconds=43200,
      AnnotationConsolidationConfig={
        AnnotationConsolidationLambdaArn='arn:aws:lambda:eu-west-1:568282634449:function:ACS-TextMultiClassMultiLabel'
      }"
}

####################################
### RESOURCE DELETION
####################################

function delete_user_pool_domain() {
  user_pool_id=$1
  aws cognito-idp delete-user-pool-domain --user-pool-id "$user_pool_id" --domain "$RESOURCE_NAME"
}

function delete_user_pool() {
  user_pool_id=$1
  aws cognito-idp delete-user-pool --user-pool-id "$user_pool_id"
}

function delete_workforce() {
  aws sagemaker delete-workteam --workteam-name "$RESOURCE_NAME"
  aws sagemaker delete-workforce --workforce-name "$RESOURCE_NAME"
}

####################################
### MAIN
####################################

function create_deps() {
  user_pool_id=$(print_user_pool_id)
  if [[ -z $user_pool_id ]]; then
    create_user_pool
    user_pool_id=$(print_user_pool_id)
  fi

  user_pool_client_id=$(print_user_pool_client_id "$user_pool_id")
  workteam_arn=$(print_workteam_arn)
  if [[ -z $workteam_arn ]]; then
    create_workforce "$user_pool_id" "$user_pool_client_id"
  fi

  configure_user_pool_client "$user_pool_id" "$user_pool_client_id"
}

function create_job() {
  workteam_arn=$(print_workteam_arn)
  create_labeling_job "$workteam_arn"
}

function delete_deps() {
  user_pool_id=$(print_user_pool_id)
  delete_workforce || echo "Workforce already deleted"
  delete_user_pool_domain "$user_pool_id" || echo "User pool domain already deleted"
  delete_user_pool "$user_pool_id" || echo "User pool already deleted"
}

VERB="${1:?"missing argument 1 for verb"}"
RESOURCE_NAME="cpv-classification"

if [[ $VERB == "create-deps" ]]; then
  create_deps
elif [[ $VERB == "create-job" ]]; then
  JOB_SUFFIX="${2:?"missing argument 1 for job suffix"}"
  S3_WORKING_BUCKET="${3:?"missing argument 1 for S3 working bucket name"}"
  S3_WORKING_PATH="${4:?"missing argument 1 for S3 working bucket path to resources"}"
  IAM_ROLE_NAME="${5:?"missing argument 1 for IAM role name"}"
  WORKER_COUNT="${6:-1}"
  ROLE_ARN="arn:aws:iam::528719223857:role/$IAM_ROLE_NAME"
  create_job
elif [[ $VERB == "delete-deps" ]]; then
  delete_deps
else
  echo "Invalid verb '$VERB', supported verbs are 'create' and 'delete'"
  exit 1
fi
